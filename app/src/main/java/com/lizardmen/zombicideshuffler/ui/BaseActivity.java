package com.lizardmen.zombicideshuffler.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.lizardmen.zombicideshuffler.App;

public abstract class BaseActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).inject(this);
    }
}
