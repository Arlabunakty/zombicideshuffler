package com.lizardmen.zombicideshuffler.ui.fragment;

import android.app.Fragment;
import android.util.Log;
import android.widget.TextView;
import com.lizardmen.zombicideshuffler.App;
import com.lizardmen.zombicideshuffler.R;
import com.lizardmen.zombicideshuffler.model.Card;
import com.lizardmen.zombicideshuffler.model.Danger;
import com.lizardmen.zombicideshuffler.model.MonsterType;
import com.lizardmen.zombicideshuffler.ui.CardViewerActivity;
import com.lizardmen.zombicideshuffler.util.FontUtil;
import org.androidannotations.annotations.*;

import javax.inject.Inject;

@EFragment(R.layout.card_slider_page)
public class CardPageFragment extends Fragment {

    @FragmentArg("cardPosition")
    int cardPosition;

    @Inject
    FontUtil fontUtil;

    @ViewById(R.id.cardId)
    TextView id;
    @ViewById(R.id.comment)
    TextView comment;
    @ViewById(R.id.blue)
    TextView blue;
    @ViewById(R.id.yellow)
    TextView yellow;
    @ViewById(R.id.orange)
    TextView orange;
    @ViewById(R.id.red)
    TextView red;

    @AfterInject
    void afterInject() {
        ((App) getActivity().getApplication()).inject(this);
    }

    @AfterViews
    void afterViews() {
        Card card = ((CardViewerActivity)getActivity()).getCards().get(cardPosition);

        id.setText("#" + card.getId());
        id.setTypeface(fontUtil.getZombicideTypeface());

        Log.d(this.getClass().getSimpleName(), "Card id: " + card.getId());

        for (Danger danger : card.getDangerLevels()) {
            Log.d(this.getClass().getSimpleName(), "Formatting danger level " + danger.getLevel());
            TextView textView;
            switch (danger.getLevel()){
                case BLUE:
                    textView = blue;
                    break;
                case YELLOW:
                    textView = yellow;
                    break;
                case ORANGE:
                    textView = orange;
                    break;
                case RED:
                    textView = red;
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported danger level " + danger.getLevel());
            }

            String text;
            MonsterType monsterType = danger.getMonsterType();
            if (danger.getMonsterNumber() > 0) {
                text = danger.getMonsterNumber() + "x" + monsterType.name().toUpperCase();
            } else if (monsterType != null &&
                    (monsterType == MonsterType.ALLRUNNERS ||
                            monsterType == MonsterType.ALLWALKERS ||
                            monsterType == MonsterType.ALLFATTIES)) {
                text = monsterType.name().toUpperCase();
            } else {
                text = "" + danger.getMonsterNumber();
            }

            textView.setText(text);
            textView.setTypeface(fontUtil.getZombicideTypeface());
        }
    }

}
