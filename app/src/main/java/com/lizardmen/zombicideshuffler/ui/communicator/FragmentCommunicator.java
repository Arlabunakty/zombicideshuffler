package com.lizardmen.zombicideshuffler.ui.communicator;

public interface FragmentCommunicator<A>{
    public void passDataToFragment(A message);
}
