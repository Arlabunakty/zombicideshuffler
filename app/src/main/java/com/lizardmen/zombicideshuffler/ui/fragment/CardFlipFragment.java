package com.lizardmen.zombicideshuffler.ui.fragment;

import android.app.Fragment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.lizardmen.zombicideshuffler.R;
import com.lizardmen.zombicideshuffler.ui.CardViewerActivity_;
import com.lizardmen.zombicideshuffler.ui.communicator.ActivityCommunicator;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.Touch;

@EFragment(R.layout.card_activity)
public class CardFlipFragment extends Fragment {

    @FragmentArg("cardPosition")
    int cardPosition;

    boolean mShowingBack = false;

    ActivityCommunicator<Boolean> activityCommunicator;

    @AfterViews
    void afterViews() {
        activityCommunicator =(CardViewerActivity_)getActivity();
        getFragmentManager()
                .beginTransaction()
                .add(R.id.container, CardFlipFragment_.CardFrontFragment_.builder().build())
                .commit();
    }

    @Touch(R.id.container)
    public void button(View v, MotionEvent event) {
        if (!mShowingBack && event.getAction() == MotionEvent.ACTION_UP) {
            Log.d(this.getClass().getSimpleName(), "Touch event");
            flipCard();
            activityCommunicator.passDataToActivity(true);
        }
    }

    private void flipCard() {
        mShowingBack = true;

        getFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out)
                .replace(R.id.container, CardPageFragment_.builder().cardPosition(cardPosition).build())
                .commit();
    }

    @EFragment(R.layout.fragment_card_front)
    public static class CardFrontFragment extends Fragment {
    }
}
