package com.lizardmen.zombicideshuffler.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import com.lizardmen.zombicideshuffler.R;
import com.lizardmen.zombicideshuffler.listener.SeasonUICheckBoxListener;
import com.lizardmen.zombicideshuffler.model.Season;
import com.lizardmen.zombicideshuffler.model.SeasonUI;
import com.lizardmen.zombicideshuffler.service.DeckService;
import com.lizardmen.zombicideshuffler.service.SeasonService;
import com.lizardmen.zombicideshuffler.ui.adapter.SeasonUIArrayAdapter;
import com.lizardmen.zombicideshuffler.ui.communicator.ActivityCommunicator;
import com.lizardmen.zombicideshuffler.ui.communicator.FragmentCommunicator;
import com.lizardmen.zombicideshuffler.ui.dialog.SeasonsSelectorDialog;
import com.lizardmen.zombicideshuffler.util.FontUtil;
import com.software.shell.fab.ActionButton;
import lombok.Setter;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import javax.inject.Inject;
import java.util.*;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity implements ActivityCommunicator<List<Season>> {

    @ViewById(R.id.seasonsListView)
    ListView seasonsListView;

    @ViewById(R.id.okButton)
    Button okButton;

    @ViewById(R.id.randomButton)
    Button randomButton;

    @ViewById(R.id.add_season_button)
    ActionButton addSeasonButton;

    @Inject
    SeasonService seasonService;

    @Inject
    FontUtil fontUtil;

    @Inject
    DeckService deckService;

    private SeasonUIArrayAdapter adapter;
    private Random generator;
    FragmentManager fragmentManager = getSupportFragmentManager();

    @Setter
    protected FragmentCommunicator fragmentCommunicator;

    @Override public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        initListAdapter();
        generator = new Random();
    }

    private void initListAdapter() {
        this.adapter = new SeasonUIArrayAdapter(this, getSeasonUIs(seasonService.getUserSeasons()));
        ((com.lizardmen.zombicideshuffler.App) getApplication()).inject(this.adapter);
    }

    @AfterViews
    void init() {
        this.adapter.setSeasonUICheckBoxListener(new SeasonUICheckBoxListener(seasonService));
        seasonsListView.setAdapter(adapter);
        okButton.setTypeface(fontUtil.getZombicideTypeface());
        randomButton.setTypeface(fontUtil.getZombicideTypeface());
        //Show/hide fab during scrolling ?!
        seasonsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_IDLE) {
                    addSeasonButton.show();
                } else {
                    addSeasonButton.hide();
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

            }
        });
        addSeasonButton.show();
    }

    protected List<SeasonUI> getSeasonUIs(List<Season> seasons) {
        List<SeasonUI> seasonUi = new ArrayList<>(seasons.size());
        Set<Season> userSelectedSeasons = seasonService.getUserSelectedSeasons();
        for (Season season : seasons) {
            SeasonUI seasonUI = new SeasonUI(season);
            seasonUI.setSelected(userSelectedSeasons.contains(season));
            seasonUi.add(seasonUI);
        }
        return seasonUi;
    }

    protected List<SeasonUI> getSeasonsUIs(List<Season> seasons, Set<Season> selectedSeasons) {
        List<SeasonUI> seasonUi = getSeasonUIs(seasons);
        for(SeasonUI season : seasonUi) {
            season.setSelected(selectedSeasons.contains(season.getSeason()));
        }
        return seasonUi;
    }

    @Click(R.id.add_season_button)
    void addSeasonButton() {
        SeasonsSelectorDialog fragment1 = new SeasonsSelectorDialog();
        fragment1.setSeasons(seasonService.getSeasons());
        fragment1.setSelected(seasonService.getUserSeasons());
        fragment1.show(fragmentManager, "Show Alert Dialog to select user seasons.");
    }

    @Override
    public void passDataToActivity(List<Season> seasons) {
        seasonService.setUserSeasons(seasons);
        adapter.clear();
        Collections.sort(seasons);
        adapter.addAll(getSeasonUIs(seasons));
        adapter.notifyDataSetChanged();
    }

    @Click(R.id.randomButton)
    public void randomiseDecksAndShuffleCards() {
        //TODO: move code to service and kepp only presentation
        List<Season> userSeasons = seasonService.getUserSeasons();
        int randomDeckNumber = generator.nextInt(userSeasons.size()) + 1;
        Set<Season> randomSeasons = new HashSet<>();
        while(randomSeasons.size() < randomDeckNumber) {
            randomSeasons.add(userSeasons.get(generator.nextInt(userSeasons.size())));
        }
        seasonService.setSelectedUserSeasons(randomSeasons);
        adapter.clear();
        adapter.addAll(getSeasonsUIs(seasonService.getUserSeasons(), randomSeasons));
        adapter.notifyDataSetChanged();
    }

    @Click(R.id.okButton)
    public void okButton() {
        List<SeasonUI> seasonUIs = adapter.getList();
        ArrayList<Season> seasons = new ArrayList<>(seasonUIs.size());
        for (SeasonUI seasonUI : seasonUIs) {
            seasons.add(seasonUI.getSeason());
        }
        CardViewerActivity_.intent(this).seasons(seasons).start();
    }
}
