package com.lizardmen.zombicideshuffler.ui.adapter;

import java.util.List;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.lizardmen.zombicideshuffler.listener.SeasonUICheckBoxListener;
import com.lizardmen.zombicideshuffler.service.SeasonService;
import com.lizardmen.zombicideshuffler.util.FontUtil;
import com.lizardmen.zombicideshuffler.R;
import com.lizardmen.zombicideshuffler.model.SeasonUI;
import lombok.Getter;
import lombok.Setter;

import javax.inject.Inject;

public class SeasonUIArrayAdapter extends ArrayAdapter<SeasonUI> {

    @Getter @Setter List<SeasonUI> list;
    private final Activity activity;
    @Inject
    FontUtil fontUtil;
    @Setter
    SeasonUICheckBoxListener seasonUICheckBoxListener;

    public SeasonUIArrayAdapter(Activity activity, List<SeasonUI> list) {
        super(activity, R.layout.season_row_button, list);
        this.activity = activity;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //TODO: reduce call number via adjusted xml layouts and reduce re-drawing elements because of non-solid elements' dimensions. PR#03.
        Log.d(this.getClass().getSimpleName(), "getView was called");
        View view;
        if (convertView == null) {
            LayoutInflater inflator = activity.getLayoutInflater();
            view = inflator.inflate(R.layout.season_row_button, parent, false);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) view.findViewById(R.id.label);
            viewHolder.checkbox = (CheckBox) view.findViewById(R.id.check);
            viewHolder.checkbox
                    .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            SeasonUI element = (SeasonUI) viewHolder.checkbox.getTag();
                            if (element.getSelected() != buttonView.isChecked()) {
                                seasonUICheckBoxListener.onCheckedChanged(buttonView, isChecked);
                                element.setSelected(buttonView.isChecked());
                            }
                        }
                    });
            view.setTag(viewHolder);
            viewHolder.checkbox.setTag(list.get(position));
        } else {
            view = convertView;
            ((ViewHolder) view.getTag()).checkbox.setTag(list.get(position));
        }
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.text.setText(list.get(position).getSeason().getTitle());
        //TODO: prefer next line to do via inheritance from {@link TextView} and using it in xml layouts. PR#09.
        holder.text.setTypeface(fontUtil.getZombicideTypeface());
        holder.checkbox.setChecked(list.get(position).getSelected());
        return view;
    }

    class ViewHolder {
        protected TextView text;
        protected CheckBox checkbox;
    }
}