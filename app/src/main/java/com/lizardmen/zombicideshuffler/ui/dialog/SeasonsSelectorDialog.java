package com.lizardmen.zombicideshuffler.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import com.lizardmen.zombicideshuffler.R;
import com.lizardmen.zombicideshuffler.model.Season;
import com.lizardmen.zombicideshuffler.service.SeasonService;
import com.lizardmen.zombicideshuffler.ui.MainActivity;
import com.lizardmen.zombicideshuffler.ui.MainActivity_;
import com.lizardmen.zombicideshuffler.ui.communicator.ActivityCommunicator;
import com.lizardmen.zombicideshuffler.ui.communicator.FragmentCommunicator;
import lombok.Setter;

import java.util.List;
import java.util.ListIterator;

public class SeasonsSelectorDialog extends DialogFragment implements FragmentCommunicator<SeasonService> {

    Context context;
    ActivityCommunicator<List<Season>> activityCommunicator;

    @Setter List<Season> seasons;
    @Setter List<Season> selected;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        context = getActivity();
        activityCommunicator =(MainActivity_)context;
        ((MainActivity)context).setFragmentCommunicator(this);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String[] items = new String[seasons.size()];
        boolean[] isChecked = new boolean[seasons.size()];
        for(ListIterator<Season> li = seasons.listIterator(); li.hasNext();) {
            int index = li.nextIndex();
            Season season = li.next();
            items[index] = season.getTitle();
            isChecked[index] = selected.contains(season);
        }

        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());

        builder.setTitle(context.getString(R.string.select_seasons))
                .setMultiChoiceItems(items, isChecked,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            public void onClick(DialogInterface dialog, int item, boolean isChecked) {
                                if (isChecked) {
                                    selected.add(seasons.get(item));
                                } else {
                                    selected.remove(selected.indexOf(seasons.get(item)));
                                }
                            }
                        });

        builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                activityCommunicator.passDataToActivity(selected);
                dismiss();
            }
        });

        builder.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        return builder.create();
    }

    @Override
    public void passDataToFragment(SeasonService service) {
        this.seasons = service.getSeasons();
        this.selected = service.getUserSeasons();
    }
}
