package com.lizardmen.zombicideshuffler.ui.communicator;

public interface ActivityCommunicator<A>{
    public void passDataToActivity(A message);
}
