package com.lizardmen.zombicideshuffler.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import com.lizardmen.zombicideshuffler.App;
import com.lizardmen.zombicideshuffler.R;
import com.lizardmen.zombicideshuffler.model.Card;
import com.lizardmen.zombicideshuffler.model.Season;
import com.lizardmen.zombicideshuffler.service.DeckService;
import com.lizardmen.zombicideshuffler.ui.communicator.ActivityCommunicator;
import com.lizardmen.zombicideshuffler.ui.fragment.CardFlipFragment_;
import com.lizardmen.zombicideshuffler.ui.fragment.CardPageFragment_;
import com.lizardmen.zombicideshuffler.ui.transformer.DepthPageTransformer;
import com.lizardmen.zombicideshuffler.ui.view.DeactivatableViewPager;
import lombok.Getter;
import org.androidannotations.annotations.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@EActivity(R.layout.card_screen_slide)
public class CardViewerActivity extends FragmentActivity implements ActivityCommunicator<Boolean>{

    @Inject
    DeckService deckService;

    @Extra("seasons")
    ArrayList<Season> seasons;

    @ViewById(R.id.pager)
    DeactivatableViewPager pager;

    @Getter
    List<Card> cards = new ArrayList<>();

    ScreenSlidePagerAdapter adapter;

    @AfterExtras
    void afterExtras() {
        ((App) getApplication()).inject(this);
        for (Season season : seasons) {
            cards.addAll(deckService.getBySeason(season).getCards());
        }
        Collections.shuffle(cards);
    }

    @AfterViews
    void afterViews() {
        adapter = new ScreenSlidePagerAdapter(getFragmentManager());
        pager.setAdapter(adapter);
        pager.setPageTransformer(true, new DepthPageTransformer());
        pager.setEnabledPaging(false);
    }

    @Override
    public void passDataToActivity(Boolean enabledPaging) {
        pager.setEnabledPaging(enabledPaging);
    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Log.d(this.getClass().getSimpleName(), "Preparing item w/ position " + position);
            if (!pager.isEnabledPaging() && position == 0) {
                return CardFlipFragment_.builder()
                        .cardPosition(position)
                        .build();
            } else {
                return CardPageFragment_.builder()
                        .cardPosition(position)
                        .build();
            }
        }

        @Override
        public int getCount() {
            return cards.size();
        }
    }
}
