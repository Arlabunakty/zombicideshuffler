package com.lizardmen.zombicideshuffler.ui.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import lombok.Getter;
import lombok.Setter;

public class DeactivatableViewPager extends ViewPager {
    public DeactivatableViewPager(Context context) {
        super(context);
    }

    public DeactivatableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Getter
    @Setter
    boolean enabledPaging;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return !isEnabledPaging() || super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return isEnabledPaging() && super.onInterceptTouchEvent(event);
    }
}
