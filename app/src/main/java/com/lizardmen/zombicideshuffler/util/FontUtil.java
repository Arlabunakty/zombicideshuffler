package com.lizardmen.zombicideshuffler.util;

import android.content.Context;
import android.graphics.Typeface;
import lombok.Getter;

public class FontUtil {

    public static final String ZOMBICIDE_FONT = "fonts/Xl3E3pLA.ttf";

    @Getter Typeface zombicideTypeface;

    private final Context context;

    public FontUtil(Context context) {
        this.context = context;
        this.zombicideTypeface = Typeface.createFromAsset(context.getAssets(), ZOMBICIDE_FONT);
    }
}
