package com.lizardmen.zombicideshuffler.listener;

import android.widget.CompoundButton;
import com.lizardmen.zombicideshuffler.model.Season;
import com.lizardmen.zombicideshuffler.model.SeasonUI;
import com.lizardmen.zombicideshuffler.service.SeasonService;

public class SeasonUICheckBoxListener implements CompoundButton.OnCheckedChangeListener {
    SeasonService seasonService;

    public SeasonUICheckBoxListener(SeasonService seasonService) {
        this.seasonService = seasonService;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        SeasonUI seasonUI = (SeasonUI) buttonView.getTag();
        Season season = seasonUI.getSeason();
        if (buttonView.isChecked()) {
            seasonService.addSelectedUserSeason(season);
        } else {
            seasonService.removeSelectedUserSeason(season);
        }
    }
}
