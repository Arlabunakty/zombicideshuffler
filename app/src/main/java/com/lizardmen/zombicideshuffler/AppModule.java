package com.lizardmen.zombicideshuffler;

import android.content.Context;
import com.google.gson.Gson;
import com.lizardmen.zombicideshuffler.annotation.ForApplication;
import com.lizardmen.zombicideshuffler.service.DeckService;
import com.lizardmen.zombicideshuffler.service.SeasonService;
import com.lizardmen.zombicideshuffler.ui.CardViewerActivity_;
import com.lizardmen.zombicideshuffler.ui.MainActivity_;
import com.lizardmen.zombicideshuffler.ui.adapter.SeasonUIArrayAdapter;
import com.lizardmen.zombicideshuffler.ui.fragment.CardPageFragment_;
import com.lizardmen.zombicideshuffler.util.FontUtil;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module(
        injects = {
                MainActivity_.class,
                CardViewerActivity_.class,
                SeasonUIArrayAdapter.class,
                CardPageFragment_.class
        },
        complete = false
)
public class AppModule {
    private final App application;

    public AppModule(App application) {
        this.application = application;
    }

    /**
     * Allow the application context to be injected but require that it be annotated with
     * {@link ForApplication @Annotation} to explicitly differentiate it from an activity context.
     */
    @Provides
    @Singleton
    @ForApplication
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    SeasonService provideSeasonService(@ForApplication Context context, Gson gson) {
        return new SeasonService(context, gson);
    }

    @Provides
    @Singleton
    FontUtil provideFontUtil(@ForApplication Context context) {
        return new FontUtil(context);
    }

    @Provides
    @Singleton
    DeckService provideDeckService(@ForApplication Context context, Gson gson) {
        return new DeckService(context, gson);
    }
}
