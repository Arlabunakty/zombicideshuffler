package com.lizardmen.zombicideshuffler.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.gson.Gson;
import com.lizardmen.zombicideshuffler.R;
import com.lizardmen.zombicideshuffler.model.Season;
import lombok.Getter;
import lombok.Setter;

import java.io.InputStreamReader;
import java.util.*;

public class SeasonService {

    public static final String TAG = SeasonService.class.getSimpleName();
    public static final String USER_SEASONS_PREF_KEY = "userSeasons";
    public static final String SELECTED_USER_SEASONS_PREF_KEY = "selectedUserSeasons";
    protected Gson gson;
    protected Context context;

    protected SharedPreferences seasonPreferences;

    protected Seasons seasons;
    protected List<Season> userSeasons = new ArrayList<>();
    protected Set<Season> userSelected = new HashSet<>();

    public SeasonService(Context context, Gson gson) {
        this.context = context;
        this.gson = gson;
        seasonPreferences = context.getSharedPreferences("seasonPreferences", Context.MODE_MULTI_PROCESS);
        load();
    }

    private void load() {
        seasons = gson.fromJson(new InputStreamReader(context.getResources().openRawResource(R.raw.seasons)), Seasons.class);
        Set<String> seasonCodes = seasonPreferences.getStringSet(USER_SEASONS_PREF_KEY, Collections.<String>emptySet());
        Log.d(TAG, "User has " + seasonCodes.size());

        for (String seasonCode : seasonCodes) {
            for (Season season : getSeasons()) {
                if (season.getCode().equals(seasonCode)) {
                    userSeasons.add(season);
                    Log.d(this.getClass().getSimpleName(), "User has season w/ code " + season.getCode());
                    break;
                }
            }
        }
        Collections.sort(userSeasons);

        Set<String> selectedSeasonCodes = getStringSetSelectedSeasons();
        Log.d(TAG, "User has userSeasons " + selectedSeasonCodes.size());
        for (String seasonCode : selectedSeasonCodes) {
            Log.d(TAG, "User has user seasons season w/ code " + seasonCode + " in prefs.");
            for (Season season : userSeasons) {
                if (season.getCode().equals(seasonCode)) {
                    Log.d(TAG, "User has user seasons season w/ code " + season.getCode());
                    userSelected.add(season);
                    break;
                }
            }
        }
    }

    public List<Season> getSeasons() {
        return seasons.getSeasons();
    }

    public List<Season> getUserSeasons() {
        return userSeasons;
    }

    public Set<Season> getUserSelectedSeasons() {
        return userSelected;
    }

    public void setUserSeasons(List<Season> userSeasons) {
        this.userSeasons = userSeasons;
        Set<String> userSeasonsCodes = new HashSet<>();
        for (Season season : userSeasons) {
            userSeasonsCodes.add(season.getCode());
        }
        seasonPreferences.edit().putStringSet(USER_SEASONS_PREF_KEY, userSeasonsCodes).commit();
    }

    public void addSelectedUserSeason(Season season) {
        Log.d(TAG, "Selecting season w/ code " + season.getCode());
        this.userSelected.add(season);
        Set<String> selectedStrings = getStringSetSelectedSeasons();
        if (selectedStrings.isEmpty()) {
            HashSet<String> strings = new HashSet<>();
            strings.add(season.getCode());
            Log.d(TAG, "Changes were committed " + seasonPreferences.edit().putStringSet(SELECTED_USER_SEASONS_PREF_KEY, strings).commit());
        } else {
            selectedStrings.add(season.getCode());
            Log.d(TAG, "Changes were committed " + seasonPreferences.edit().putStringSet(SELECTED_USER_SEASONS_PREF_KEY, selectedStrings).commit());
        }

        Log.d(TAG, "User has selected seasons " + getStringSetSelectedSeasons().size());
    }

    public void removeSelectedUserSeason(Season season) {
        Log.d(TAG, "Removing season w/ code " + season.getCode());
        this.userSelected.remove(season);
        Set<String> selectedStrings = getStringSetSelectedSeasons();
        selectedStrings.remove(season.getCode());
        Log.d(TAG, "Changes were committed " + seasonPreferences.edit().putStringSet(SELECTED_USER_SEASONS_PREF_KEY, selectedStrings).commit());
        Log.d(TAG, "User has selected seasons " + getStringSetSelectedSeasons().size());
    }

    private Set<String> getStringSetSelectedSeasons() {
        return seasonPreferences.getStringSet(SELECTED_USER_SEASONS_PREF_KEY, Collections.<String>emptySet());
    }

    public void setSelectedUserSeasons(Set<Season> selectedUserSeasons) {
        this.userSelected = selectedUserSeasons;
        Set<String> strings = new HashSet<>();
        for (Season season : selectedUserSeasons) {
            strings.add(season.getCode());
        }
        Log.d(TAG, "Changes were committed " + seasonPreferences.edit().putStringSet(SELECTED_USER_SEASONS_PREF_KEY, strings).commit());
    }
}

class Seasons {
    @Getter @Setter List<Season> seasons;
}