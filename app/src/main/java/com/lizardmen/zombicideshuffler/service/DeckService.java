package com.lizardmen.zombicideshuffler.service;

import android.content.Context;
import com.google.gson.Gson;
import com.lizardmen.zombicideshuffler.R;
import com.lizardmen.zombicideshuffler.model.Card;
import com.lizardmen.zombicideshuffler.model.Deck;
import com.lizardmen.zombicideshuffler.model.Season;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;

public class DeckService {
    protected Gson gson;
    protected Context context;

    public static final HashMap<String, Integer> seasonBk2FileId = new HashMap<>();

    static {
        seasonBk2FileId.put("season1", R.raw.season1);
        seasonBk2FileId.put("season2", R.raw.season2);
        seasonBk2FileId.put("season3", R.raw.season3);
        seasonBk2FileId.put("addonTCM", R.raw.addontcm);
        seasonBk2FileId.put("addonAN", R.raw.addonan);
        seasonBk2FileId.put("wotd1", R.raw.wotd1);
        seasonBk2FileId.put("wotd2", R.raw.wotd2);
        seasonBk2FileId.put("zdogs", R.raw.zdogs);
        seasonBk2FileId.put("tcrowd", R.raw.tcrowd);
        seasonBk2FileId.put("azombies", R.raw.azombies);
        seasonBk2FileId.put("lzombivors", R.raw.izombivors);
        seasonBk2FileId.put("vip1", R.raw.vip1);
        seasonBk2FileId.put("vip2", R.raw.vip2);
        seasonBk2FileId.put("motc", R.raw.motc);
    }

    public DeckService(Context context, Gson gson) {
        this.context = context;
        this.gson = gson;
    }

    public Deck getBySeason(Season season) {
        InputStream deckInputStream = context.getResources().openRawResource(seasonBk2FileId.get(season.getCode()));
        return gson.fromJson(new InputStreamReader(deckInputStream), Deck.class);
    }
}
