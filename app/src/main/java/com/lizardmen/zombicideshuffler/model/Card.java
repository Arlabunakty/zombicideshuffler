package com.lizardmen.zombicideshuffler.model;

import lombok.Getter;

import java.util.List;

public class Card {
    @Getter Long id;
    @Getter CardType type;
    @Getter List<Danger> dangerLevels;
}
