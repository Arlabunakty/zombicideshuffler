package com.lizardmen.zombicideshuffler.model;

import android.os.Parcel;
import android.os.Parcelable;
import lombok.Getter;

public class Season implements Parcelable, Comparable<Season> {
    @Getter String title;
    @Getter String code;
    @Getter int displayOrder;

    public Season(String title, String code) {
        this.title = title;
        this.code = code;
    }

    public Season(Parcel parcel) {
        String[] data = new String[2];
        parcel.readStringArray(data);

        this.code = data[0];
        this.title = data[1];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {this.code, this.title});
    }

    public static final Parcelable.Creator<Season> CREATOR = new Parcelable.Creator<Season>() {

        @Override
        public Season createFromParcel(Parcel source) {
            return new Season(source);
        }

        @Override
        public Season[] newArray(int size) {
            return new Season[0];
        }
    };

    @Override
    public int hashCode() {
        return this.code.hashCode();
    }

    @Override
    public boolean equals(Object c) {
        return c instanceof Season && ((Season) c).code.equals(code);
    }

    @Override
    public int compareTo(Season another) {
        if (this.displayOrder > another.getDisplayOrder()) {
            return 1;
        } else if(this.displayOrder == another.getDisplayOrder()) {
            return 0;
        } else {
            return -1;
        }
    }
}
