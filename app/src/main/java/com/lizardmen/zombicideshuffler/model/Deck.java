package com.lizardmen.zombicideshuffler.model;

import lombok.Getter;

import java.util.List;

public class Deck {
    @Getter Season season;
    @Getter List<Card> cards;
}
