package com.lizardmen.zombicideshuffler.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Denys on 06.09.2015.
 */
public class SeasonUI {
    @Getter Season season;
    @Getter @Setter Boolean selected;

    public SeasonUI(Season season) {
        this.season = season;
        this.selected = false;
    }
}
