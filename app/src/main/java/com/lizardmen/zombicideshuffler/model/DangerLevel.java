package com.lizardmen.zombicideshuffler.model;

import com.google.gson.annotations.SerializedName;

public enum DangerLevel {
    @SerializedName("blue") BLUE,
    @SerializedName("yellow") YELLOW,
    @SerializedName("orange") ORANGE,
    @SerializedName("red") RED
}
