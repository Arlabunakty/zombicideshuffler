package com.lizardmen.zombicideshuffler.model;

import lombok.Getter;

public class Danger {
    @Getter DangerLevel level;
    @Getter Long monsterNumber;
    @Getter MonsterType monsterType;
}
