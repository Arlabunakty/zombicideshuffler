package com.lizardmen.zombicideshuffler.model;

import com.google.gson.annotations.SerializedName;

public enum MonsterType {
    @SerializedName("walker") WALKER,
    @SerializedName("runner") RUNNER,
    @SerializedName("fatty") FATTY,
    @SerializedName("abomination") ABOMINATION,
    @SerializedName("allrunners")ALLRUNNERS,
    @SerializedName("allwalkers")ALLWALKERS,
    @SerializedName("allfatties")ALLFATTIES,
    @SerializedName("vipz") VIP_ZOMBIE,
    @SerializedName("seeker") SEEKER,
    @SerializedName("dog") DOG,
    @SerializedName("lost") LOST,
    @SerializedName("crowz") CROW
}
